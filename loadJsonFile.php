<?php

require_once("load.php");

$loadDataService = new LoadDataService();
$arrFileContents = $loadDataService->loadJsonFile(PROJECT . DS . "inputFile" . DS . "Code Challenge (Sales).json");

if ($arrFileContents) {
    $dbData = $loadDataService->getDbData($arrFileContents);

    $customer = new CustomerController();
    $customer->insertNewCustomer($dbData["customers"]);
    echo ("<h1> Insert customers done !!! </h1>");

    $product = new ProductController();
    $product->insertNewProduct($dbData["products"]);
    echo ("<h1> iInsert products done !!! </h1>");

    $sale = new SaleController();
    $sale->insertNewSale($dbData["sales"]);
    echo ("<h1> Insert sales done !!! </h1>");
} else {
    echo ("<h1> Json data error !!! </h1>");
}
