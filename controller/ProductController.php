<?php

/**
 * ProductController class
 */
class ProductController extends BaseController
{

    protected $model = "Product";

    /**
     * Insert New Product into database
     *
     * @param array $products
     * @return void
     */
    function insertNewProduct($products)
    {

        $this->loadMoedl($this->model);

        $this->Product->insert($products);
    }
}
