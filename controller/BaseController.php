<?php

/**
 * BaseController class
 */
class BaseController
{

    /**
     * Allows you to load a model
     *
     * @param string $name
     * @return void
     */
    public function loadMoedl($name)
    {
        $file = PROJECT . DS . 'model' . DS . $name . '.php';
        require_once($file);
        if (!isset($this->$name)) {
            $this->$name = new $name();
        }
    }
}
