<?php

/**
 * SaleController class
 */
class SaleController extends BaseController
{

    protected $model = "Sale";

    /**
     * Insert New sale into database
     *
     * @param array $sales
     * @return void
     */
    function insertNewSale($sales)
    {
        $this->loadMoedl($this->model);
        $this->Sale->insert($sales);
    }

    /**
     * Get sale list with/without filters
     *
     * @param array $request
     * @return void
     */
    public function getList($request)
    {
        $this->loadMoedl($this->model);
        return $this->Sale->getList($request);
    }
}
