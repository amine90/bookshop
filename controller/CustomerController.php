<?php

/**
 * CustomerController class
 */
class CustomerController extends BaseController
{

    /**
     * Model Name
     *
     * @var string
     */
    protected $model = "Customer";

    /**
     * Insert New Customer into database
     *
     * @param array $customers
     * @return void
     */
    function insertNewCustomer($customers)
    {

        $this->loadMoedl($this->model);

        $this->Customer->insert($customers);
    }
}
