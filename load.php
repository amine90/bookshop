<?php

define('WEBROOT' , dirname(__FILE__)); 
define('ROOT' , dirname(WEBROOT));
define('DS' , DIRECTORY_SEPARATOR); 
define('PROJECT' , ROOT.DS."bookShop"); 

// load database configuration
require PROJECT.DS.'config'.DS.'conf.php';

// load controller
require_once(PROJECT.DS."controller".DS."BaseController.php");
require_once(PROJECT.DS."controller".DS."CustomerController.php");
require_once(PROJECT.DS."controller".DS."ProductController.php");
require_once(PROJECT.DS."controller".DS."SaleController.php");

// load model
require_once(PROJECT.DS."model".DS."BaseModel.php");

// load services
require_once(PROJECT.DS."service".DS."LoadDataService.php");