<?php

class LoadDataService
{
    /**
     * get json file data
     *
     * @param string $path
     * @return array
     */
    public function loadJsonFile($path)
    {
        $jsonFileContents = file_get_contents($path);

        if ($jsonFileContents === false) {
            echo 'Json File Contents error !!!';
            return false;
        }

        $arrFileContents = json_decode($jsonFileContents, true);
        if ($arrFileContents === null) {
            echo 'Json decode error !!!';
            return false;
        }

        return $arrFileContents;
    }

    /**
     * get Database data (clean data)
     *
     * @param [array $fileData
     * @return array
     */
    public function getDbData($fileData)
    {
        $customers = [];
        $products = [];
        $sales = [];

        $idCustomer = 0;
        $mailCustomers = [];
        $idProducts = [];

        foreach ($fileData as $row) {

            // customer data
            $existCustomer = $this->customers($row, $mailCustomers, $customers, $idCustomer);

            // product data
            $existProduct = $this->products($row, $idProducts, $products);

            // sale data
            $sales[] = $this->sales($row, $existCustomer, $idCustomer);
        }

        return ['customers' => $customers, 'products' => $products, 'sales' => $sales];
    }

    /**
     * Prepare cutomers data
     *
     * @param array $row
     * @param array $mailCustomers
     * @param array $customers
     * @param integer $idCustomer
     * @return void
     */
    protected function customers($row, &$mailCustomers, &$customers, &$idCustomer)
    {
        $existCustomer = array_search($row["customer_mail"], $mailCustomers);
        if ($existCustomer === false) {
            $idCustomer++;
            $customers[] = [
                "id" => $idCustomer,
                "name" => $row["customer_name"],
                "mail" => $row["customer_mail"],
            ];
            $mailCustomers[$idCustomer] = $row["customer_mail"];
        }
        return $existCustomer;
    }

    /**
     * Prepare product data
     *
     * @param array $row
     * @param integer $idProducts
     * @param array $products
     * @return void
     */
    protected function products($row, &$idProducts, &$products)
    {
        $existProduct = array_search($row["product_id"], $idProducts);
        if ($existProduct === false) {
            $products[] = [
                "id" => $row["product_id"],
                "name" => $row["product_name"],
            ];
            $idProducts[] = $row["product_id"];
        }
        return $existProduct;
    }

    /**
     * Prepare sale data
     *
     * @param array $row
     * @param integer $existCustomer
     * @param integer $idCustomer
     * @return void
     */
    protected function sales($row, $existCustomer, $idCustomer)
    {
        return [
            "id" => $row["sale_id"],
            "customer_id" => ($existCustomer) ? $existCustomer : $idCustomer,
            "product_id" => $row["product_id"],
            "date" => $row["sale_date"],
            "product_price" => $row["product_price"],
        ];
    }
}
