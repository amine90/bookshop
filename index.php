<?php

require_once("load.php");

$sale = new SaleController();
$result = $sale->getList($_POST);

$totalPrice = 0;
if (!empty($result)) {
    $result = json_decode(json_encode($result), true);
    $totalPrice = array_sum(array_column($result, "price"));
}

?>
<form action="index.php" method="POST">

    <label for="customer">Customer:</label>
    <input type="text" id="customer" name="customer" minlength="4" maxlength="150" value="<?= (isset($_POST['customer'])) ? $_POST['customer'] : '' ?>">

    <label for="Product">Product:</label>
    <input type="text" id="product" name="product" minlength="4" maxlength="150" value="<?= (isset($_POST['product'])) ? $_POST['product'] : '' ?>">

    <label for="price">Price:</label>
    <input type="text" id="price" name="price" value="<?= (isset($_POST['price'])) ? $_POST['price'] : '' ?>">

    <input type="submit" value="Search">
</form>

<table>
    <thead>
        <tr>
            <th colspan="3">Result</th>
        </tr>
        <tr>
            <th>customer</th>
            <th>product</th>
            <th>price</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($result as $key => $row) : ?>
            <tr>
                <td><?= $row["c_name"] ?></td>
                <td><?= $row["p_name"] ?></td>
                <td><?= $row["price"] ?></td>
            </tr>
        <?php endforeach ?>

        <tr>
            <th colspan="2">Total Proce : </th>
            <td><?= $totalPrice ?></td>
        </tr>
    </tbody>
</table>