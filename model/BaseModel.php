<?php

/**
 * BaseModel class
 */
abstract class BaseModel
{

    /**
     * Configuration
     *
     * @var string
     */
    public $conf = 'default';

    /**
     * Database object
     *
     * @var PDO
     */
    public $db;

    /**
     * create pdo db object
     */
    function __construct()
    {

        $conf = Conf::$database[$this->conf];

        try {
            $dsn = 'mysql:host=' . $conf['host'] . ';dbname=' . $conf['database'] . ';';
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
            $pdo = new PDO($dsn, $conf['login'], $conf['password'], $options);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            $this->db =  $pdo;
        } catch (PDOException $e) {
            die('Unable to connect to the database');
        }
    }

    /**
     * Get values and binding Parameters data (Insert Query function)
     *
     * @param array $data
     * @param array $bindParam
     * @return void
     */
    abstract function getValuesInsertQuery($data, &$bindParam);

    /**
     * Generic insert function
     *
     * @param array $data
     * @return void
     */
    public function insert($data)
    {

        $bindParam = [];
        $values = $this->getValuesInsertQuery($data, $bindParam);
        $query = 'INSERT INTO ' . $this->table . ' (' . implode(' , ', $this->metaData) . ') VALUES ' . implode(' , ', $values) . ';';

        $stmt = $this->db->prepare($query);
        return  $stmt->execute($bindParam);
    }

    /**
     * Get basic select query
     *
     * @param array $fields
     * @return string
     */
    public function getQueryBuilder($fields = [])
    {
        $query = 'SELECT ';
        if (count($fields) > 0) {
            if (is_array($fields)) {
                $query .= implode(', ', $fields);
            } else {
                $query .= $fields;
            }
        } else {
            $query .= '*';
        }

        return $query . ' FROM ' . $this->table . ' as ' . $this->alias . ' ';
    }

    /**
     * Get Select Result ()
     *
     * @param string $query
     * @param boolean $fetchAll
     * @return array
     */
    public function getResult($query, $fetchAll = true)
    {
        $pre = $this->db->prepare($query);
        $pre->execute();

        if ($fetchAll) {
            return $pre->fetchAll(PDO::FETCH_OBJ);
        }

        return $pre->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Get join query
     *
     * @param string $alias1
     * @param string $pk1
     * @param string $table2
     * @param string $alias2
     * @param string $fk2
     * @param string $type
     * @return void
     */
    protected function getInnerJoin($alias1, $pk1, $table2, $alias2, $fk2, $type = 'INNER')
    {
        return " INNER JOIN $table2 $alias2 ON $alias1.$pk1 = $alias2.$fk2 ";
    }
}
