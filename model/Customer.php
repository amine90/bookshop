<?php

class Customer extends BaseModel
{

    /**
     * Metadata of customer table
     *
     * @var array
     */
    public $metaData = [
        "id",
        "name",
        "mail"
    ];

    /**
     * Table name
     *
     * @var string
     */
    public $table = "Customer";

    /**
     * Alias of table
     *
     * @var string
     */
    public $alias = "c";

    /**
     * Primary key
     *
     * @var string
     */
    public $primary = "id";

    /**
     * Get values and binding Parameters data (Insert Query function)
     *
     * @param array $data
     * @param array $bindParam
     * @return array
     */
    public function getValuesInsertQuery($data, &$bindParam)
    {
        $values = [];
        foreach ($data as $key => $row) {
            $id = ':id' . $key;
            $name = ':name' . $key;
            $mail = ':mail' . $key;

            $bindParam[$id] = $row['id'];
            $bindParam[$name] = $row['name'];
            $bindParam[$mail] = $row['mail'];

            array_push($values, "($id, $name, $mail)");
        }

        return $values;
    }
}
