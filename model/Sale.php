<?php

class Sale extends BaseModel
{
    /**
     * Metadata of Sale table
     *
     * @var array
     */
    public $metaData = [
        "id",
        "customer_id",
        "product_id",
        "date",
        "product_price"
    ];

    /**
     * Table name
     *
     * @var string
     */
    public $table = "Sale";

    /**
     * Alias of table
     *
     * @var string
     */
    public $alias = "s";

    /**
     * Primary key
     *
     * @var string
     */
    public $primary = "id";

    /**
     * Get values and binding Parameters data (Insert Query function)
     *
     * @param array $data
     * @param array $bindParam
     * @return array
     */
    public function getValuesInsertQuery($data, &$bindParam)
    {
        $values = [];
        foreach ($data as $key => $row) {
            $id = ':id' . $key;
            $customerId = ':customer_id' . $key;
            $productId = ':product_id' . $key;
            $date = ':date' . $key;
            $productPrice = ':product_price' . $key;

            $bindParam[$id] = $row['id'];
            $bindParam[$customerId] = $row['customer_id'];
            $bindParam[$productId] = $row['product_id'];
            $bindParam[$date] = $row['date'];
            $bindParam[$productPrice] = $row['product_price'];

            array_push($values, "($id, $customerId, $productId, $date, $productPrice)");
        }

        return $values;
    }

    /**
     * Get sale list with/without filters
     *
     * @param [type] $filters
     * @return void
     */
    public function getList($filters){
        $query = $this->getQueryBuilder(["c.name as c_name", "p.name as p_name", "s.product_price as price"]);
        $customerJoin = $this->getInnerJoin($this->alias, "customer_id", "customer", "c", "id");
        $productJoin = $this->getInnerJoin($this->alias, "product_id", "product", "p", "id");
        $query = $query. $customerJoin . $productJoin . $this->getCondition($filters);

        return $this->getResult($query);
    }

    /**
     * Get where condition from filters
     *
     * @param array $filters
     * @return void
     */
    protected function getCondition($filters){

        $condition = [];

        if(isset($filters["customer"]) && !empty($filters["customer"])){
            $condition[] = "c.name like '%".$filters["customer"]."%'";
        }

        if(isset($filters["product"]) && !empty($filters["product"])){
            $condition[] = "p.name like '%".$filters["product"]."%'";
        }

        if(isset($filters["price"]) && !empty($filters["price"]) && is_numeric($filters["price"])){
            $condition[] = "s.price = ".$filters["price"];
        }

        if(count($condition) > 0){
            return "WHERE " . implode(" AND ", $condition);
        }

        return "";
    }
}
