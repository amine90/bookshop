<?php

class Product extends BaseModel
{
    /**
     * Metadata of Product table
     *
     * @var array
     */
    public $metaData = [
        "id",
        "name"
    ];

    /**
     * Table name
     *
     * @var string
     */
    public $table = "Product";

    /**
     * Alias of table
     *
     * @var string
     */
    public $alias = "p";

    /**
     * Primary key
     *
     * @var string
     */
    public $primary = "id";

    /**
     * Get values and binding Parameters data (Insert Query function)
     *
     * @param array $data
     * @param array $bindParam
     * @return array
     */
    public function getValuesInsertQuery($data, &$bindParam)
    {
        $values = [];
        foreach ($data as $key => $row) {
            $id = ':id' . $key;
            $name = ':name' . $key;

            $bindParam[$id] = $row['id'];
            $bindParam[$name] = $row['name'];

            array_push($values, "($id, $name)");
        }

        return $values;
    }
}
